# muonrc_busystats

Scripts to plot the busy of each of the muon subsystems

## Run
The CI pipeline that will trigger the generation of the plots will be triggered at every commit. If you want to push a commit without running the pipeline put at the end of your commit message the following phrase "Bypass-pipeline.".

The CI pipeline will re-run on all runs and will re-generate all the plots whenever there is a commit pushed that updatest the code itself. If however the change is only adding one or more run numbers in the RunLists/\*.txt the pipeline will only process the new runs to save time.

If you only need to add new runs to the list to produce the plots you can directly update the RunList2022.txt from the browser. After you select the RunLists/\*.txt file in the gitlab project page, click Open in Web IDE, add the new number(s), click Create commit, select Commit to master branch and the click commit. This trigger the pipeline that will run the scripts and save the new plots in the Plots folder


## Authors and acknowledgment
Code developed by Philipp Fleischmann (philipp.fleischmann@cern.ch)

