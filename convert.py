#!/usr/bin/python

import sys
from datetime import datetime

epoch = datetime.utcfromtimestamp(0)

def unix_time(dt):
    return (dt - epoch).total_seconds()

fobj = open(sys.argv[1])
for line in fobj:
    l=line.split()
    print("%d %s" % (unix_time(datetime.strptime(l[1]+" "+l[2], "%d-%m-%Y %H:%M:%S:%f")), l[3]))
fobj.close()
