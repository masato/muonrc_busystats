#!/usr/bin/env bash

diffList=$1

export REPO=`echo $CI_PROJECT_URL | sed -e s#https://##`
git remote set-url origin https://${CI_USER}:${CI_PUSH_TOKEN}@$REPO.git
git config user.email ${CI_EMAIL}
git config user.name ${CI_USER}
git add Plots
if [ -s $diffList ]
then
    git commit -m "All plots updated $(date +'%Y-%m-%d %H:%M:%S') Bypass-pipeline." && git push|| true 
else
    git commit -m "Plots added $(date +'%Y-%m-%d %H:%M:%S') Bypass-pipeline." && git push|| true
fi



